
#ifndef TOPAZ_H
#define TOPAZ_H

#include "common.h"

void topaz_polling_tags(struct picc_device *picc);
int topaz_xfr_handler(struct picc_device *picc, uint8_t *cmdBuf, uint32_t cmdLen, uint8_t *resBuf, uint32_t *resLen);

#endif
