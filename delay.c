
#include "common.h"
#include "pn51x.h"

#ifdef PCD_TIMER_USE_PIT
uint32_t pcd_tick = 0;
static void PCD_TIMER_ISR(pointer p)
{
    _pcd_clear_int(PCD_TIMER_INT);
    /* increase tick */
    pcd_tick ++;
   /* Enable PIT interrupt neeed due to errata on Kinetis PIT */
   _pcd_timer_unmask_int(PCD_TIMER_INT);
}

static void pcd_timer_init(void)
{
    if(_int_install_isr(PCD_TIMER_INT, PCD_TIMER_ISR, NULL) == NULL) {
        return;
    } /* Endif */
    _bsp_int_init(AUDIO_INT, 2, 0, TRUE);
}
#endif

void Delay1us(uint32_t delay)
{
#ifdef PCD_TIMER_USE_PIT
    uint32_t savedTime = PIT_CVAL(TIMER_FOR_PCD);
    uint32_t currentTime = savedTime;
    uint32_t endTime = savedTime + (delay * (PCD_TIMER_FREQUENCY / 1000000));

    while (currentTime <= savedTime){
        savedTime = currentTime;
        currentTime = PIT_CVAL(TIMER_FOR_PCD);
    }

    while ((currentTime > endTime) && (currentTime <= savedTime)) {
        savedTime = currentTime;
        currentTime = PIT_CVAL(TIMER_FOR_PCD);
    }
#else
    unsigned long  ovfl;
    MQX_TICK_STRUCT cur_tick,last_tick;
    _time_get_elapsed_ticks_fast(&cur_tick);

    do {
        _time_get_elapsed_ticks_fast(&last_tick);
    } while( _time_diff_milliseconds(&last_tick,&cur_tick,&ovfl) >= delay);
#endif

}

void Delay256us(uint8_t delay)
{
    volatile uint8_t i;

    while (delay--) {
        Delay1us(200);
        for(i = 0; i < 134; i++);
    }
}

void Delay256P2us(uint8_t delay)
{
    uint8_t i;
    
    for(i = 0; i < delay; i++) {
        Delay256us(0xFF);
        Delay256us(0x01);
    }
}

void Delay256P3us(uint8_t delay)
{
    uint8_t i;
    
    for(i = 0; i < delay; i++)
    {
        Delay256P2us(0xFF);
        Delay256P2us(0x01);
    }
}

void Delay1ms(uint32 delay)
{
    OS_Time_delay(delay + 2);
}

void Delay1s(uint8_t  delay)
{
    uint8_t i;
    for(i = 0; i < delay; i++) {
        Delay1ms(1000);
    }
}

void SetTimer100us(uint16_t timeOut)
{
    set_pn51x_timer(timeOut);
}


