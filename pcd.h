
#ifndef _PCD_H_
#define _PCD_H_

#include "common.h"
#include "picc.h"

struct pcd_param
{
    uint8_t *p_iBuf;
    uint8_t *p_oBuf;
    uint32_t  iDataLen;
    uint32_t  oDataLen;
    uint32_t statusCode;
};

#define  Card_PowerOn     0x01
#define  Card_PowerOff    0x02
#define  Card_XfrAPDU     0x03

//void run_picc_poll(struct work_struct *work);
//DECLARE_DELAYED_WORK(card_Poll, run_picc_poll);
struct pcd_common
{
    struct pcd_device		pcd;
    struct picc_device		picc;

    OS_Mutex_handle	mutex;
    uint8_t	sem_inc;
    uint32_t polling_task_handle;
    int 		(*slot_changed_notify)(void *, uint8_t);
    void		*private_data;
};

int pcd_init(void);
void pcd_deinit(void);
extern struct pcd_common  *common;
 

#endif

