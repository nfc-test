
#ifndef DELAY_H
#define DELAY_H

#include "common.h"

//#define PCD_TIMER_USE_PIT


#ifdef  PCD_TIMER_USE_PIT
#define TIMER_FOR_PCD     0

#define PCD_TIMER_FREQUENCY      1000000
#define PCD_TIMER     BSP_BUS_CLOCK

#define PCD_TIMER_INT                   BSP_PIT1_INT_VECTOR
#define PCD_TIMER                           BSP_LAST_TIMER
#define _pcd_timer_mask_int           _pit_mask_int
#define _pcd_timer_unmask_int         _pit_unmask_int
#define _pcd_timer_init_freq          _pit_init_freq
#define _pcd_clear_int                _pit_clear_int
#endif

void SetTimer100us(uint16_t timeOut);
void Delay1s(uint8_t delay);
void Delay1ms(uint32_t delay);
void Delay256P3us(uint8_t delay);
void Delay256P2us(uint8_t delay);
void Delay256us(uint8_t delay);
void Delay1us(uint32_t delay);

#endif







