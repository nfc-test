
#include "common.h"

#define CRC_A    1
#define CRC_B    2

void CRCReg_Update(uint8_t ch, uint16_t *IpwCrc)
{
    ch ^= (uint8_t)(*IpwCrc & 0x00FF);
    ch ^= (ch << 4);
    *IpwCrc = (*IpwCrc >> 8)^((uint16_t)ch << 8)^((uint16_t)ch << 3)^((uint16_t)ch >> 4);
}

void ComputeCrc(uint8_t CRCType, uint8_t *Data, uint32_t Length, uint8_t *TransmitFirst, uint8_t *TransmitSecond)
{
    uint16_t wCrc;

    switch(CRCType) 
    {
        case CRC_A:
            wCrc = 0x6363;        // ITU-V.41
            break;
        case CRC_B:
            wCrc = 0xFFFF;        // ISO/IEC 13239 (formerly ISO/IEC 3309)
            break;
        default:
            return;
    }

    while(Length--) {
        CRCReg_Update(*Data++, &wCrc);
    }

    if (CRCType == CRC_B) {
        wCrc = ~wCrc;         // ISO/IEC 13239 (formerly ISO/IEC 3309)
    }
    
    *TransmitFirst = (uint8_t)(wCrc & 0xFF);
    *TransmitSecond = (uint8_t)((wCrc >> 8) & 0xFF);
}

