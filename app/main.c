/*HEADER**********************************************************************
*
* Copyright 2012 Freescale Semiconductor, Inc.
*
* This software is owned or controlled by Freescale Semiconductor.
* Use of this software is governed by the Freescale MQX RTOS License
* distributed with this Material.
* See the MQX_RTOS_LICENSE file distributed for more details.
*
* Brief License Summary:
* This software is provided in source form for you to use free of charge,
* but it is not open source software. You are allowed to use this software
* but you cannot redistribute it or derivative works of it in source form.
* The software may be used only in connection with a product containing
* a Freescale microprocessor, microcontroller, or digital signal processor.
* See license agreement file for full license terms including other
* restrictions.
*****************************************************************************
*
* Comments:
*
*   This file contains the source for a simple example of an
*   application that access the picc with pcd.
*
*
*END************************************************************************/


#include <string.h>
#include "../pcd.h"
#include "../mifare.h"

extern void main_task (uint32_t);

/* note :
 the last block in the one sector is cortrol block.  (such as 3, 7 ...)
 0x00x00x00x00x00x00xff0x70x80 0x69 0xff 0xff0xff0xff0xff0xff
*/
#define TEST_BLOCK_NR   0x6


const TASK_TEMPLATE_STRUCT  MQX_template_list[] =
{
    /* Task Index,   Function,   Stack,  Priority,   Name,   Attributes,          Param, Time Slice */
    { 10L,          main_task,  2500L,  10L,         "Main", MQX_AUTO_START_TASK, 0,     0  },
    { 0 }
};


/*TASK*-------------------------------------------------------------------
*
* Task Name : main_task
* Comments  :
*
*END*----------------------------------------------------------------------*/
void main_task
   (
      uint32_t dummy
   )
{
    // test read mifare 1
    int32_t ret,i;
    uint8_t *senBuf = NULL;
    uint32_t senLen = 256;
    uint8_t *recBuf = NULL; 
    uint32_t recLen = 256;
    uint8_t level = 0;
        
    struct pcd_common *p_common = NULL;
    pcd_init();

    ret = pn51x_reg_read(VersionReg);
    printf("main pn51x version %d\n",ret);

    p_common = common;

    senBuf = OS_Mem_alloc_zero(senLen);
    recBuf = OS_Mem_alloc_zero(recLen);

    CLEAR_BIT(p_common->pcd.flags_polling, AUTO_POLLING);
    // polling (anticolision ,and select card)
    if((ret = picc_power_on(&p_common->picc, recBuf, &recLen)) != 0) {
        goto err;
    }

    if(!BITISSET(p_common->picc.status, PRESENT)) {
        goto err;
    }

    if(p_common->picc.type == PICC_MIFARE) {
    // load key
        senLen = 11;
        senBuf[0] = 0xff;
        senBuf[1] = 0x82;
        senBuf[2] = 0x00;
        senBuf[3] = 0x01;
        senBuf[4] = 0x06;
        for(i =0 ; i<(int)(senBuf[4]);i++)
            senBuf[i + 5] = 0xff;

        if(picc_command_exchange(&p_common->picc, senBuf, senLen, recBuf /* max 256bytes */, &recLen,&level)) {
            goto err;
        }

        // authen 
        // FF 86 00 00 05 ADB
        senLen = 10;
        senBuf[0] = 0xff;
        senBuf[1] = 0x86;
        senBuf[2] = 0x00;
        senBuf[3] = 0x00;
        senBuf[4] = 0x05;
        senBuf[5] = 0x00;
        
        senBuf[7] = TEST_BLOCK_NR;   //block
        senBuf[8] = PICC_MF_KEY_A;  //key type
        senBuf[9] = 0x00;   // key number (0,1)
                
        if(picc_command_exchange(&p_common->picc, senBuf, senLen, recBuf /* max 256bytes */, &recLen,&level)) {
            goto err;
        }

#if 1
        // test write block
        // FF B0 00 BLOCK_NO LE
        senLen = 5;
        senBuf[0] = 0xff;
        senBuf[1] = 0xD6;
        senBuf[2] = 0x00;
        senBuf[3] = TEST_BLOCK_NR; // block number
        senBuf[4] = 0x10; // LE
         for(i =0 ; i<(int)(senBuf[4]);i++)
            senBuf[i + 5] = 0xa5;
        if(picc_command_exchange(&p_common->picc, senBuf, senLen, recBuf /* max 256bytes */, &recLen,&level)) {
            goto err;
        }
#endif        

         memset(senBuf,0x0,senLen);
         memset(recBuf,0x0,recLen);

        // read block
        // FF B0 00 BLOCK_NO LE
        senLen = 5;
        senBuf[0] = 0xff;
        senBuf[1] = 0xB0;
        senBuf[2] = 0x00;
        senBuf[3] = TEST_BLOCK_NR; // block number
        senBuf[4] = 0x10; // LE

        if(picc_command_exchange(&p_common->picc, senBuf, senLen, recBuf /* max 256bytes */, &recLen,&level)) {
            goto err;
        }
        pr_debug("read block number 1 ,16byte :\n");
        for(i = 0; i < recLen-2; i++) {
            pr_debug("0x%x",recBuf[i]);
        }
        pr_debug("\n");
    }
    
    if(&p_common->picc)	
        picc_power_off(&p_common->picc);

    printf("mifare 1 test ok ,plese reset and test again\n");
    pcd_deinit();
    return;
    
err:    
    pcd_deinit();
    printf("mifare 1 test failed\n");
}
