
#include "pcd.h"



#define CARD_POLLING_TASK_DEF_PRIORITY  10
#define CARD_POLLING_TASK_STACKSIZE    2500

BOOL card_poll = TRUE;
struct pcd_common  *common = NULL;


#include "picc.c"
#include "ccid_picc.c"

#if  0
static long pcd_ioctl(u32 cmd, unsigned long arg)
{
    struct pcd_common *common = ;
    uint8_t pcd_cmd = (cmd >> 4) & 0xFF;
    struct pcd_param *UsrParam = (struct pcd_param *)arg;
    uint8_t *p_iData;
    uint8_t *p_oData;
    uint32_t  ret = 0;
    uint8_t level = 0;

    if(OS_Sem_wait(common->mutex)) {    // acquire the semaphore
        ret = -ERESTARTSYS;
        goto err;
    }

    if((!UsrParam)) {
        ret = -EFAULT;          // bad address
        goto err;
    }

    switch(pcd_cmd)
    {		
        case Card_PowerOn:
        {
            if(!UsrParam->p_oBuf) {
                ret = -EFAULT;       // bad address
                goto err;
            }
			
            if(!p_oData) {
                ret = -EFAULT;       // bad address
                goto err;                
            }

            if((ret = picc_power_on(&common->picc, UsrParam->p_oBuf, &UsrParam->oDataLen)) != 0)	
                goto err/*2*/;
			
            break; 
        }

        case Card_PowerOff:
        {
            picc_power_off(&common->picc);
            ret = 0;
            break;
        }

        case Card_XfrAPDU:
        {
            if((UsrParam->iDataLen <= 0) || (UsrParam->oDataLen <= 0) || (!UsrParam->p_iBuf) || (!UsrParam->p_oBuf)) {
                ret = -EFAULT;       // bad address
                goto err;
            }
			
		
            if((ret = picc_command_exchange(&common->picc, UsrParam->p_iBuf, UsrParam->iDataLen, UsrParam->p_oBuf, &UsrParam->oDataLen, &level)) != 0)
                goto err;

            if((UsrParam->oDataLen <= 0)) {
                ret = -EFAULT;       // bad address
                goto err/*1*/;
            }
		
            break;
        }

        default:
            break;
    }

    OS_Sem_post(&common->mutex); 
    return(0);

err:
    OS_Sem_post(&common->mutex);                    // release the semaphore
    UsrParam->statusCode = ret;
    return(ret);
}


static int pcd_open()
{
    if(common->sem_inc > 0)    return(-ERESTARTSYS);
    common->sem_inc++;

    return(0);
}

static int pcd_release()
{
    struct pcd_common *common = filp->private_data;
    common->sem_inc--;
    
    return(0);
}
#endif

extern int picc_interrput_in(uint8_t slot_status);

void run_picc_poll(void * param)
{
    while(card_poll){
    if(!OS_Sem_wait(common->mutex,1)) {
        goto done;
        //return;
    }

    if(BITISSET(common->pcd.flags_polling, AUTO_POLLING) && 
        BITISSET(common->pcd.flags_polling, POLLING_CARD_ENABLE)) {
        picc_polling_tags(&common->picc);
        if(BITISSET(common->picc.status, SLOT_CHANGE)) {
            if(!picc_interrput_in(common->picc.status & PRESENT))
                CLEAR_BIT(common->picc.status, SLOT_CHANGE);
        }
    }

    OS_Sem_post(common->mutex);

done:
    OS_Time_delay(common->pcd.poll_interval);
    }
}

/*static*/ int pcd_init(void)  /* todo return pcd handle */
{
    int ret;

    common = OS_Mem_alloc_zero(sizeof (struct pcd_common));
    if (!common) {
        ret = -ENOMEM;
        goto err1;
    }

    common->mutex = OS_Sem_create(/*1*/0);    // initial a semaphore, and lock it

    ret = picc_init(common);
    if(ret)
        goto err2;

    // register IO
    /*
    if(ret) {
        pr_err("fail to register device\n");
        goto err3;
    }*/

#if 0
    card_poll = TRUE;
    common->polling_task_handle = OS_Task_create(run_picc_poll, (void*)NULL, (uint32_t)CARD_POLLING_TASK_DEF_PRIORITY, 
        CARD_POLLING_TASK_STACKSIZE, "card polling task", NULL);
    
    if(common->polling_task_handle ==  (uint32_t)OS_TASK_ERROR) {
        pr_err("can't create polling task\n");
        ret = -EFAULT;
        goto err4;
    }
#endif    

    OS_Sem_post(common->mutex);

    return (0);

err4:
    // IO unregister
err3:
    picc_uninit();
err2:	
    OS_Sem_post(&common->mutex);
    OS_Mem_free(common);
err1:	
	
    return ret;
}

/*static*/ void pcd_deinit(void)
{
    card_poll = FALSE;
    OS_Time_delay(2);
    if(common->polling_task_handle) {
        OS_Task_delete(common->polling_task_handle);
        common->polling_task_handle = NULL;
    }

    if (common->mutex) {
        OS_Sem_destroy(common->mutex);
        return;
    }
    
    picc_uninit();
    // io unregister	
  
    OS_Mem_free(common);
	
    return;
}


