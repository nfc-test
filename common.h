

#ifndef COMMON_H
#define COMMON_H
#include <mqx.h>
#include <bsp.h>
#include <posix.h>

#include "lwevent.h"
#include "lwmsgq.h"
#include "mutex.h"
#include "lwsem.h"

#define DEBUG_NFC

typedef void * HANDLE;
typedef uint8_t    BOOL;


#ifndef TRUE
#define TRUE   1
#endif

#ifndef FALSE
#define FALSE  0
#endif

#define __compiler_offsetof(a,b) __builtin_offsetof(a,b)
#define offsetof(TYPE,MEMBER) __compiler_offsetof(TYPE,MEMBER)

//-#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)
#define container_of(ptr, type, member) ({			\
	const typeof( ((type *)0)->member ) *__mptr = (ptr);	\
	(type *)( (char *)__mptr - offsetof(type,member) );})


#define SET_BIT(reg, mask)       (reg  |= mask)
#define CLEAR_BIT(reg, mask)     (reg  &= (~mask))
#define TOGGLE_BIT(reg, mask)    (reg  ^= mask)
#define BITISSET(reg, mask)      (reg  &  mask)
#define BITISCLEAR(reg, mask)    ((reg &  mask) == 0)


#define MAKEWORD(msb, lsb)      (((uint16_t)msb <<8) | (uint16_t)lsb)
#define MAKEUINT32(msb,midb,mida,lsb)      (((uint32_t)msb << 24) | ((uint32_t)midb << 16)\
                                         | ((uint32_t)mida << 8) | (uint32_t)lsb)

#define BITS_PER_LONG 32
#define BIT(nr)			(1UL << (nr))
#define BIT_MASK(nr)		(1UL << ((nr) % BITS_PER_LONG))
#define BIT_WORD(nr)		((nr) / BITS_PER_LONG)
#define BITS_PER_BYTE		8
#define DIV_ROUND_UP(n,d) (((n) + (d) - 1) / (d))
#define BITS_TO_LONGS(nr)	DIV_ROUND_UP(nr, BITS_PER_BYTE * sizeof(long))


/* for debug message */
#ifndef pr_fmt
#define pr_fmt(fmt) fmt
#endif

#define __stringify_1(x...)	#x
#define __stringify(x...)	__stringify_1(x)

#define halt() do { for(;;); } while (0)
#define ASSERT(x) do { if (!(x)) { /*dbgPrintf*/printf("%s:%d ASSERT failed: %s\n", __FILE__, __LINE__, #x); halt(); } } while (0)

#if defined(DEBUG_NFC)
#define pr_debug(fmt, ...) \
	printf(pr_fmt(fmt), ##__VA_ARGS__)
#define pr_warning(fmt, ...) \
	printf(pr_fmt(fmt), ##__VA_ARGS__)
#define pr_warn pr_warning	
#else
#define pr_debug(fmt, ...)
#define pr_warning(fmt, ...)
#define pr_warn	
#endif
/* alway print error message */
#define pr_err(fmt, ...)    printf(pr_fmt(fmt), ##__VA_ARGS__)





/* OS API of task,memory,sem,mutex and event */
#if defined(DEBUG_NFC)
#define OS_Printf(fmt, ...) \
	printf(pr_fmt(fmt), ##__VA_ARGS__)
#else
#define OS_Printf(fmt, ...)
#endif


#if MQX_USE_UNCACHED_MEM && PSP_HAS_DATA_CACHE
    #define OS_Mem_alloc_uncached(n)            _mem_alloc_system_uncached(n)
    #define OS_Mem_alloc_uncached_zero(n)    _mem_alloc_system_zero_uncached(n)
    #define OS_Mem_alloc_uncached_align(n,a) _mem_alloc_align_uncached(n,a)
#else
    #define OS_Mem_alloc_uncached(n)            _mem_alloc_system(n)
    #define OS_Mem_alloc_uncached_zero(n)    _mem_alloc_system_zero(n)
    #define OS_Mem_alloc_uncached_align(n,a) _mem_alloc_align(n,a)
#endif   /*  PSP_HAS_DATA_CACHE */

#define OS_Mem_alloc(n)                         _mem_alloc_system(n)
#define OS_Mem_alloc_zero(n)                _mem_alloc_system_zero(n)
#define OS_Mem_free(ptr)                        _mem_free(ptr)
#define OS_Mem_zero(ptr,n)                      _mem_zero(ptr,n)
#define OS_Mem_copy(src,dst,n)                  _mem_copy(src,dst,n)

#define OS_Memcpy		memcpy
#define OS_Memset		memset

/**HEADER********************************************************************
* 
* Copyright (c) 2013 Freescale Semiconductor;
* All Rights Reserved
*
*
***************************************************************************  
*
* THIS SOFTWARE IS PROVIDED BY FREESCALE "AS IS" AND ANY EXPRESSED OR 
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  
* IN NO EVENT SHALL FREESCALE OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
* INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
* THE POSSIBILITY OF SUCH DAMAGE.
*
**************************************************************************
*
* $FileName: osadapter_mqx.c$
* $Version : 
* $Date    : 
*
* Comments:
*
* @brief  The file includes the implementation of MQX of OS adapter.
*****************************************************************************/
typedef void (* TASK_START)( void *);
typedef void (* OSA_INT_ISR_FPTR)(void *);
typedef void* OS_Event_handle;
typedef void* OS_MsgQ_handle;
typedef void* OS_GPIO_handle;
typedef void* OS_Mutex_handle;
typedef void* OS_Sem_handle;


#define OS_TASK_OK         (0)
#define OS_TASK_ERROR      (-1)
#define OS_EVENT_OK        (0)
#define OS_EVENT_ERROR     (-1)
#define OS_EVENT_TIMEOUT   (-2)
#define OS_MSGQ_OK         (0)
#define OS_MSGQ_ERROR      (-1)
#define OS_GPIO_OK         (0)
#define OS_GPIO_ERROR      (-1)
#define OS_MUTEX_OK        (0)
#define OS_MUTEX_ERROR     (-1)
#define OS_SEM_OK          (0)
#define OS_SEM_ERROR       (-1)
#define OS_SEM_TIMEOUT     (-2)

/* Block the reading task if msgq is empty */
#define OS_MSGQ_RECEIVE_BLOCK_ON_EMPTY       (0x04)
static inline uint32_t OS_Task_create(TASK_START pstart, void* param, uint32_t pri, uint32_t stack_size, char* task_name, void* opt)
{
    _task_id task_id;
    TASK_TEMPLATE_STRUCT task_template;

    task_template.TASK_TEMPLATE_INDEX = 0;
    task_template.TASK_ADDRESS = (TASK_FPTR)pstart;
    task_template.TASK_STACKSIZE = stack_size;
    task_template.TASK_PRIORITY = pri;
    task_template.TASK_NAME = task_name;
    if (opt != NULL)
    {
        task_template.TASK_ATTRIBUTES = *((uint32_t*)opt);
    }
    else
    {
        task_template.TASK_ATTRIBUTES = 0;
    }
    
    task_template.CREATION_PARAMETER = (uint32_t)param;
    task_template.DEFAULT_TIME_SLICE = 0;

    task_id = _task_create_blocked(0, 0, (uint32_t)&task_template);
    
    if (task_id == MQX_NULL_TASK_ID) {
        return (uint32_t)OS_TASK_ERROR;
    }
    
    _task_ready(_task_get_td(task_id));
    return (uint32_t)task_id;
}

static inline uint32_t OS_Task_delete(uint32_t task_id)
{
    uint32_t ret;
    ret = _task_destroy((_task_id)task_id);
    if (ret != MQX_OK)
        return (uint32_t)OS_TASK_ERROR;
    else
        return (uint32_t)OS_TASK_OK;
}

static inline  uint32_t OS_Task_suspend(uint32_t task_id)
{
    task_id = task_id;
    _task_block();
    return (uint32_t)OS_TASK_OK;
}

static inline uint32_t OS_Task_resume(uint32_t task_id)
{
    _task_ready(_task_get_td(task_id));
    return (uint32_t)OS_TASK_OK;
}

static inline void OS_Task_yield()
{
	_sched_yield();
}

static inline OS_Event_handle OS_Event_create(uint32_t flag)
{
    LWEVENT_STRUCT *event;
    event = (LWEVENT_STRUCT*)_mem_alloc_system_zero(sizeof(LWEVENT_STRUCT));
    if (event == NULL)
    {
        return NULL;
    }
    
    if (_lwevent_create(event, flag) != MQX_OK)
    {
        _mem_free((void*)event);
        return NULL;
    }
    return (OS_Event_handle)event;
}

static inline uint32_t OS_Event_destroy(OS_Event_handle handle)
{
    LWEVENT_STRUCT *event = (LWEVENT_STRUCT*)handle;
    if(_lwevent_destroy(event) != MQX_OK)
    {
        _mem_free((void*)event);
        return (uint32_t)OS_EVENT_ERROR;
    }
    _mem_free((void*)event);
    return OS_EVENT_OK; 
}

static inline uint32_t OS_Event_check_bit(OS_Event_handle handle, uint32_t bitmask)
{
	LWEVENT_STRUCT *event = (LWEVENT_STRUCT*)handle;
	if((event->VALUE & bitmask) != 0)
		return 1;
	else
		return 0;
}

static inline uint32_t OS_Event_clear(OS_Event_handle handle, uint32_t bitmask)
{
    LWEVENT_STRUCT *event = (LWEVENT_STRUCT*)handle;
    if(_lwevent_clear(event, bitmask) != MQX_OK)
    {
        return (uint32_t)OS_EVENT_ERROR;
    }
    return (uint32_t)OS_EVENT_OK;
}

static inline uint32_t OS_Event_set(OS_Event_handle handle, uint32_t bitmask)
{
    LWEVENT_STRUCT *event = (LWEVENT_STRUCT*)handle;
    if(_lwevent_set(event, bitmask) != MQX_OK)
    {
        return (uint32_t)OS_EVENT_ERROR;
    }
    return (uint32_t)OS_EVENT_OK;
}

static inline uint32_t OS_Event_wait(OS_Event_handle handle, uint32_t bitmask, uint32_t flag, uint32_t timeout)
{
    LWEVENT_STRUCT *event = (LWEVENT_STRUCT*)handle;
    uint32_t ret;

    ret = _lwevent_wait_ticks(event, bitmask, flag, timeout * _time_get_ticks_per_sec() / 1000);
    //printf("os 0x%x\n", ret);
    if(LWEVENT_WAIT_TIMEOUT == ret)
    {
        return (uint32_t)OS_EVENT_TIMEOUT;
    }
    else if(MQX_OK == ret)
    {
        return (uint32_t)OS_EVENT_OK;
    }
    return (uint32_t)OS_EVENT_ERROR;
}

static inline uint32_t OS_Event_status(OS_Event_handle handle)
{
    LWEVENT_STRUCT *event = (LWEVENT_STRUCT*)handle;
    return event->VALUE;
}

static inline OS_MsgQ_handle OS_MsgQ_create(uint32_t max_msg_number, uint32_t msg_size)
{
    void* msgq;
    uint32_t size = sizeof(LWMSGQ_STRUCT) + max_msg_number * msg_size * 4;
    
    msgq = _mem_alloc_system_zero(size);
    if (msgq == NULL)
    {
        return NULL;
    }

    if (_lwmsgq_init(msgq, max_msg_number, msg_size) != MQX_OK)
    {
        _mem_free(msgq);
        return NULL;
    }
       
    return (OS_MsgQ_handle)msgq;
}

static inline uint32_t OS_MsgQ_send(OS_MsgQ_handle msgq, void* msg, uint32_t flag)
{
    if (MQX_OK != _lwmsgq_send(msgq, (uint32_t *) msg, flag))
    {
        return (uint32_t)OS_MSGQ_ERROR;
    }
    return (uint32_t)OS_MSGQ_OK;
}

static inline uint32_t OS_MsgQ_recv(OS_MsgQ_handle msgq, void* msg, uint32_t flag, uint32_t timeout)
{
    if (MQX_OK != _lwmsgq_receive(msgq, (uint32_t *) msg, flag, timeout, NULL))
    {
        return (uint32_t)OS_MSGQ_ERROR;
    }
    return (uint32_t)OS_MSGQ_OK;
}

static inline uint32_t OS_MsgQ_Is_Empty(OS_MsgQ_handle msgq, void* msg)
{
	uint32_t ret;
	ret = LWMSGQ_IS_EMPTY(msgq);
	if(!ret)
	{
		if (MQX_OK != _lwmsgq_receive(msgq, (uint32_t *) msg, OS_MSGQ_RECEIVE_BLOCK_ON_EMPTY, 1, NULL))
		{
			return (uint32_t)OS_MSGQ_ERROR;
		}
	}
	return ret;
}

static inline uint32_t OS_MsgQ_destroy(OS_MsgQ_handle msgq)
{
    _lwmsgq_deinit(msgq);
    _mem_free(msgq);
    return (uint32_t)OS_MSGQ_OK;
}

static inline OS_Mutex_handle OS_Mutex_create()
{
	MUTEX_STRUCT_PTR mutex = NULL;
	mutex = _mem_alloc_system_zero(sizeof(MUTEX_STRUCT));
	if (mutex == NULL)
	{
	    return NULL;
	}
	if (_mutex_init(mutex, NULL) != MQX_OK)
	{
		_mem_free(mutex);
		return NULL;
	}
	return (OS_Mutex_handle)mutex;
}

static inline uint32_t OS_Mutex_lock(OS_Mutex_handle mutex)
{
	if (_mutex_lock((MUTEX_STRUCT_PTR)mutex) != MQX_OK)
	    return (uint32_t)OS_MUTEX_ERROR;
	else
		return (uint32_t)OS_MUTEX_OK;
}

static inline uint32_t OS_Mutex_unlock(OS_Mutex_handle mutex)
{
	if (_mutex_unlock((MUTEX_STRUCT_PTR)mutex) != MQX_OK)
		return (uint32_t)OS_MUTEX_ERROR;
    else
	    return (uint32_t)OS_MUTEX_OK;
}

static inline  uint32_t OS_Mutex_destroy(OS_Mutex_handle mutex)
{
    _mutex_destroy((MUTEX_STRUCT_PTR)mutex);
	_mem_free(mutex);
	return OS_MUTEX_OK;
}

static inline OS_Sem_handle OS_Sem_create(uint32_t initial_number)
{
	LWSEM_STRUCT_PTR sem = NULL;
	sem = (LWSEM_STRUCT_PTR)_mem_alloc_system_zero(sizeof(LWSEM_STRUCT));
    if (sem == NULL)
    {
        return NULL;
    }
    if (_lwsem_create(sem, initial_number) != MQX_OK)
    {
        _mem_free(sem);
        return NULL;
    }
    return (OS_Sem_handle)sem;
}

static inline uint32_t OS_Sem_wait(OS_Sem_handle sem, uint32_t timeout)
{
    uint32_t result = _lwsem_wait_ticks((LWSEM_STRUCT_PTR)sem, timeout);
    if (result == MQX_LWSEM_WAIT_TIMEOUT)
    {
        return (uint32_t)OS_SEM_TIMEOUT;
    }
    else if (result == MQX_OK)
    {
        return (uint32_t)OS_SEM_OK;
    }
    else
    {
        return (uint32_t)OS_SEM_ERROR;
    }
}

static inline uint32_t OS_Sem_post(OS_Sem_handle sem)
{
    uint32_t result = _lwsem_post((LWSEM_STRUCT_PTR)sem);
    if (result == MQX_OK)
    {
        return (uint32_t)OS_SEM_OK;
    }
    else
    {
        return (uint32_t)OS_SEM_ERROR;
    }
}

static inline uint32_t OS_Sem_destroy(OS_Sem_handle sem)
{
    uint32_t result = _lwsem_destroy((LWSEM_STRUCT_PTR)sem);
    _mem_free(sem);
    if (result == MQX_OK)
    {
        return (uint32_t)OS_SEM_OK;
    }
    else
    {
        return (uint32_t)OS_SEM_ERROR;
    }
}

void OS_Time_delay(register uint32_t milliseconds)
{
    _time_delay(milliseconds);
    return ;
}

#endif

