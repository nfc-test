
#include "common.h"
#include "nfc_spi.h"

#define NFC_CHANNEL "spi1:"
//#define NFC_SPI_GPIO_CS     11
// MQX_FILE_PTR           spi_fd = NULL;

/*
const char *clock_mode[] =
{
    "SPI_CLK_POL_PHA_MODE0",
    "SPI_CLK_POL_PHA_MODE1",
    "SPI_CLK_POL_PHA_MODE2",
    "SPI_CLK_POL_PHA_MODE3"
};

const char *device_mode[] =
{
    "SPI_DEVICE_MASTER_MODE",
    "SPI_DEVICE_SLAVE_MODE",
};
*/


#ifdef NFC_SPI_GPIO_CS

/*FUNCTION*---------------------------------------------------------------
*
* Function Name : set_CS
* Comments  : This function sets chip select signal to enable/disable memory.
*             It's used only on platforms with manual CS handling.
*END*----------------------------------------------------------------------*/
_mqx_int set_CS (uint32_t cs_mask, void * user_data)
{
    LWGPIO_STRUCT_PTR spigpio = (LWGPIO_STRUCT_PTR)user_data;

    if (cs_mask & NFC_SPI_GPIO_CS) {
        lwgpio_set_value(spigpio, LWGPIO_VALUE_LOW);
    }
    else {
        lwgpio_set_value(spigpio, LWGPIO_VALUE_HIGH);
    }
    return MQX_OK;
}
#endif /* NFC_SPI_GPIO_CS */

int  nfc_spi_write(void *spi, uint8_t *in_buf, uint32_t in_len)
{
    _mqx_int result;
    uint32_t 	temp;
    unsigned long flags;

    if(in_len <= 0)
        return -EINVAL;

    ASSERT (spi!= NULL);

    // TODO
    /* Write data */
    result = fwrite (in_buf, 1, (long)in_len, spi);
    /* Deactivate CS */
    fflush (spi);
    return result;
}

int nfc_spi_write_then_read(void * spi, uint8_t *in_buf, uint32_t in_len,
										uint8_t *out_buf, uint32_t out_len)
{
    uint32_t temp;
    unsigned long flags;
    
    //TODO
    uint32_t i;
    _mqx_int result;
    //uint_8 buffer[5];

    //printf ("Reading %d bytes from location 0x%08x in memory: ", size, addr);

    result = fwrite (in_buf, 1, in_len, spi);

    if (result != in_len) {
        /* Stop transfer */
        fflush (spi);
        printf ("ERROR (tx)\n");
        return -1;
    }

    //fflush (spi); 
    
    /* Read size bytes of data */
    result = fread (out_buf, 1, (_mqx_int)out_len, spi);

    /* De-assert CS */
    fflush (spi);

    if (result != out_len) {
        printf ("ERROR (rx)\n");
        return -1;
    }
 
    //return in_len;
    return 0;
}

int nfc_spi_init(void ** spi_fd)
{
    int result;
#ifdef NFC_CHANNEL
    LWGPIO_STRUCT          spigpio;
    SPI_CS_CALLBACK_STRUCT callback;
#endif
    uint32_t                param;

    void * spi_handle = NULL;

    // TODO
    /* Open the SPI driver */
    spi_handle = fopen (NFC_CHANNEL, NULL);

    if (NULL == spi_handle ) {
        pr_err ("Error opening SPI driver!\n");
        _time_delay (200L);
        _task_block ();
    }

#if NFC_SPI_GPIO_CS
#error liutest
    /* Open GPIO file containing SPI pin SS == chip select for memory */
    if (!lwgpio_init(&spigpio, NFC_SPI_GPIO_CS, LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_NOCHANGE)) {
        pr_err("Initializing GPIO with associated pins failed.\n");
        _time_delay (200L);
        _task_block();
    }
    lwgpio_set_functionality(&spigpio,BSP_SPI_MUX_GPIO);/*BSP_SPI_MUX_GPIO need define in BSP for function mux as GPIO*/

    /* Set CS callback */

    callback.CALLBACK = set_CS;
    callback.USERDATA = &spigpio;
    printf ("Setting CS callback ... ");
    if (SPI_OK == ioctl (spi_handle, IO_IOCTL_SPI_SET_CS_CALLBACK, &callback)) {
        pr_debug ("OK\n");
    }
    else {
        pr_debug ("ERROR\n");
    }
#endif

    /* Set a different rate */
    param = 5000000;//500000;
    printf ("Changing the baud rate to %d Hz ... ", param);
    if (SPI_OK == ioctl (spi_handle, IO_IOCTL_SPI_SET_BAUD, &param))
    {
        printf ("OK\n");
    }
    else
    {
        printf ("ERROR\n");
    }

    /* Display baud rate */
    printf ("Current baud rate ... ");
    if (SPI_OK == ioctl (spi_handle, IO_IOCTL_SPI_GET_BAUD, &param)) {
        printf ("%d Hz\n", param);
    }
    else {
        printf ("ERROR\n");
    }

    /* Set clock mode */
    param = SPI_CLK_POL_PHA_MODE0;
    printf ("Setting clock mode to %d ... ", param);
    if (SPI_OK == ioctl (spi_handle, IO_IOCTL_SPI_SET_MODE, &param)) {
        printf ("OK\n");
    }
    else {
        printf ("ERROR\n");
    }

    /* Get clock mode */
    printf ("Getting clock mode ... ");
    if (SPI_OK == ioctl (spi_handle, IO_IOCTL_SPI_GET_MODE, &param)) {
        printf ("%d\n", param);
    }
    else
    {
        printf ("ERROR\n");
    }

#if 1
    /* Set big endian */
    param = SPI_DEVICE_BIG_ENDIAN;
    printf ("Setting endian to %s ... ", param == SPI_DEVICE_BIG_ENDIAN ? "SPI_DEVICE_BIG_ENDIAN" : "SPI_DEVICE_LITTLE_ENDIAN");
    if (SPI_OK == ioctl (spi_handle, IO_IOCTL_SPI_SET_ENDIAN, &param)) {
        printf ("OK\n");
    }
    else {
        printf ("ERROR\n");
    }

    /* Get endian */
    printf ("Getting endian ... ");
    if (SPI_OK == ioctl (spi_handle, IO_IOCTL_SPI_GET_ENDIAN, &param)) {
        printf ("%s\n", param == SPI_DEVICE_BIG_ENDIAN ? "SPI_DEVICE_BIG_ENDIAN" : "SPI_DEVICE_LITTLE_ENDIAN");
    }
    else {
        printf ("ERROR\n");
    }
#endif    

    /* Set transfer mode */
    param = SPI_DEVICE_MASTER_MODE;
    printf ("Setting transfer mode to %d ... ", param);
    if (SPI_OK == ioctl (spi_handle, IO_IOCTL_SPI_SET_TRANSFER_MODE, &param)) {
        printf ("OK\n");
    }
    else {
        printf ("ERROR\n");
    }

    /* Get transfer mode */
    printf ("Getting transfer mode ... ");
    if (SPI_OK == ioctl (spi_handle, IO_IOCTL_SPI_GET_TRANSFER_MODE, &param)) {
        printf ("%d\n", param);
    }
    else {
        printf ("ERROR\n");
    }

    /* Clear statistics */
    printf ("Clearing statistics ... ");
    result = ioctl (spi_handle, IO_IOCTL_SPI_CLEAR_STATS, NULL);
    if (SPI_OK == result) {
        printf ("OK\n");
    }
    else if (MQX_IO_OPERATION_NOT_AVAILABLE == result) {
        printf ("not available, define BSPCFG_ENABLE_SPI_STATS\n");
    }
    else {
        printf ("ERROR\n");
    }

    *spi_fd = spi_handle;
    printf("int spi_handle 0x%x\n",spi_handle);

    return 0;

err:
    return result;
}

void nfc_spi_uninit(void * spi_fd)
{
    if(spi_fd) {
        fclose(spi_fd);
    }
}





