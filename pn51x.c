
/* NXP pn51x NFC driver */

#include "common.h"
#include "pn51x.h"
#include "pcd_config.h"

#include "nfc_spi.h"

#ifdef HANDLE_INTR_IN_TASK
// pn51x extern interrupt task parameters
#define NFC_TASK_NUM_MESSAGES         16
#define NFC_TASK_TEMPLATE_INDEX       0
#define NFC_TASK_ADDRESS              _nfc_intr_task
#define NFC_TASK_STACKSIZE            1600
#define NFC_TASK_NAME                 "NFC Task"
#define NFC_TASK_ATTRIBUTES           0
#define NFC_TASK_CREATION_PARAMETER   0
#define NFC_TASK_DEFAULT_TIME_SLICE   0
#define NFC_TASK_PRIORITY      8
#endif

#define PN51X_EXT_INTR_LEVEL                        5

#define PN51X_FIFO_MAX_DEEPTH			        64
#define PN51X_FIFO_MAX_FIFO_ALLOWED		62
#define PN51X_FIFO_WATER_LEVEL			58

#if 0
#define PN51X_RESET_PIN        (GPIO_PORT_A | GPIO_PIN14)
#define  PN51X_RESET_PIN_MUX_GPIO LWGPIO_MUX_A14_GPIO
#else
#define  PN51X_RESET_PIN        (GPIO_PORT_B | GPIO_PIN9)
#define  PN51X_RESET_PIN_MUX_GPIO   1
#endif

/* otg transceiver external intrrupt ,falling mode */
#if  (MQX_CPU == PSP_CPU_MK60D100M) || (MQX_CPU == PSP_CPU_MK60DF120M)
	#define PN51X_EXT_INT_PIN			(GPIO_PORT_A | GPIO_PIN26)
	#define PN51X_EXT_MUX_IRQ			LWGPIO_MUX_A26_GPIO
#elif  (MQX_CPU == PSP_CPU_MK70F120M)
#error liutest do not support k70
	#define PN51X_EXT_INT_PIN			(GPIO_PORT_B | GPIO_PIN6)
	#define PN51X_EXT_MUX_IRQ			LWGPIO_MUX_B6_GPIO
#else
	#error "#INT pin of PN51X not handled"
#endif

struct pn51x_common *pn51x_dev = NULL;

static void pn51x_interrupt(void * data);
static uint32_t _nfc_task_create(void * data);
static void _nfc_intr_task(void * data);

static int pn51x_reset(void)
{
    if (!lwgpio_init(&pn51x_dev->reset_pin, PN51X_RESET_PIN, LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_NOCHANGE)) {
        printf("Initializing test pin GPIO as output failed.\n");
        _task_block();
    }
    lwgpio_set_functionality(&pn51x_dev->reset_pin, PN51X_RESET_PIN_MUX_GPIO);

    // pn51x hardware reset (spec P99 the signal must be LOW for at least 100 ns )
    //----  400us(min)-------- 
    //     |__________| low
    //
    lwgpio_set_value(&pn51x_dev->reset_pin,1);
    OS_Time_delay(50);
    lwgpio_set_value(&pn51x_dev->reset_pin,0);
    OS_Time_delay(2);
    lwgpio_set_value(&pn51x_dev->reset_pin,1);

    return 0;
}

static int pn51x_init_extintr(void)
{
    if (!lwgpio_init(&pn51x_dev->intr_pin, PN51X_EXT_INT_PIN, LWGPIO_DIR_INPUT, LWGPIO_VALUE_NOCHANGE)) {
        printf("Initializing button 1 GPIO as input failed.\n");
        _task_block();
    }

    lwgpio_set_functionality(&pn51x_dev->intr_pin, PN51X_EXT_MUX_IRQ);
    lwgpio_set_attribute(&pn51x_dev->intr_pin, LWGPIO_ATTR_PULL_UP, LWGPIO_AVAL_ENABLE);

    /* enable gpio functionality for given pin, react on falling edge */
    if (!lwgpio_int_init(&pn51x_dev->intr_pin, LWGPIO_INT_MODE_FALLING)) {
        printf("Initializing button GPIO for interrupt failed.\n");
        _task_block();
    }

    /* install gpio interrupt service routine */
    _int_install_isr(lwgpio_int_get_vector(&pn51x_dev->intr_pin), pn51x_interrupt, (void *) pn51x_dev);
    /* set the interrupt level, and unmask the interrupt in interrupt controller*/
    _bsp_int_init(lwgpio_int_get_vector(&pn51x_dev->intr_pin), PN51X_EXT_INTR_LEVEL, 0, TRUE);  /* intr level is 5 */
    /* enable interrupt on GPIO peripheral module */
    lwgpio_int_enable(&pn51x_dev->intr_pin, TRUE);
    
    return 0;
}

int pn51x_reg_write(uint8_t reg, uint8_t value)
{
    uint8_t cmdBuf[2];
    int ret;
    // we removed this lock ,mqx spi io driver will do lock at internal !
    //OS_Mutex_lock(pn51x_dev->pn51x_lock);

    reg <<= 1;
    CLEAR_BIT(reg, BIT(7));
    cmdBuf[0] = reg;
    cmdBuf[1] = value;

    ret = nfc_spi_write(pn51x_dev->spi_handle, cmdBuf, 2);
    if(ret < 0) {
        pr_err("writting register to pn51x failed\n");
        goto done;
    }

    ret = 0;

done:
    //OS_Mutex_unlock(pn51x_dev->pn51x_lock);
    return(0);
}

uint8_t pn51x_reg_read(uint8_t reg)
{
    uint8_t tempReg;
    uint8_t tempVal = 0x00;
    int ret;

    //OS_Mutex_lock(pn51x_dev->pn51x_lock);
    tempReg = (reg << 1);
    SET_BIT(tempReg, BIT(7));

    ret = nfc_spi_write_then_read(pn51x_dev->spi_handle, &tempReg, 1, &tempVal, 1);
    if(ret < 0) {
        pr_err("reading register from pn51x failed\n");
    }

    //OS_Mutex_unlock(pn51x_dev->pn51x_lock);
    return(tempVal);
}

void pn51x_reg_clear(uint8_t reg, uint8_t bitMask)
{
	uint8_t tempValue;

	tempValue = pn51x_reg_read(reg);
	tempValue &= ~bitMask;
	pn51x_reg_write(reg, tempValue);
}

void pn51x_reg_set(uint8_t reg, uint8_t bitMask)
{
	uint8_t tempValue;
    
	tempValue = pn51x_reg_read(reg);
	tempValue |= bitMask;
	pn51x_reg_write(reg, tempValue);
}

int pn51x_fifo_write(uint8_t *data, uint32_t len)
{
    uint8_t buf[65];  /* */
    int ret;

    //OS_Mutex_lock(pn51x_dev->pn51x_lock);

    if (len > PN51X_FIFO_MAX_DEEPTH) {
        ret = -EINVAL;
        goto done;
    }

    if(len <= 0) {
        ret = 0;
        goto done;
    }
    
    buf[0] = (FIFODataReg << 1) & 0x7E;     // accroding to PN512: bit 7 = 0, write; bit 0 = 0
    memcpy(buf + 1, data, len);

    ret = nfc_spi_write(pn51x_dev->spi_handle, buf, len + 1);
    if(ret < 0)
        goto done;

    ret = 0;

done:
    //OS_Mutex_unlock(pn51x_dev->pn51x_lock);
    return(ret);
}

int pn51x_fifo_read(uint8_t *data, uint32_t len)
{
    uint32_t i;
    uint8_t buf[64];
    int ret;
    //OS_Mutex_lock(pn51x_dev->pn51x_lock);
    
    if (len > PN51X_FIFO_MAX_DEEPTH) {
       	ret = -EINVAL;
		goto done;
    }
    
    if (len <= 0) {
        ret = 0;
        goto done;
    }
    
    memset(buf, (FIFODataReg << 1) | 0x80, len);    // accroding to PN512: bit 7 = 1, read; bit 0 = 0

    for(i = 0; i < len; i++) {
        ret = nfc_spi_write_then_read(pn51x_dev->spi_handle, buf+i, 1, data+i, 1);
        if(ret < 0) {
            goto done;
        }
    }
    ret = 0;

done:
    //OS_Mutex_unlock(pn51x_dev->pn51x_lock);
    return(ret);
}


void set_pn51x_timer(uint16_t timeOut)
{
    pn51x_reg_write(TModeReg, 0x82);                        //  TAuto=1,TAutoRestart=0,TPrescaler=677=2a5h
    //printf("TModeReg 0x%x\n",pn51x_reg_read(TModeReg));
    pn51x_reg_write(TPrescalerReg, 0xA5);                   //  Indicate 100us per timeslot
    pn51x_reg_write(TReloadVal_Hi, (uint8_t)(timeOut>>8));  	// 
    pn51x_reg_write(TReloadVal_Lo, (uint8_t)timeOut);        //
    pn51x_reg_write(CommIRqReg, 0x01);                      // Clear the TimerIrq bit
}

void turn_on_antenna(void)
{
	pn51x_reg_write(TxControlReg, 0x83);
}

void turn_off_antenna(void)
{
	pn51x_reg_write(TxControlReg, 0x80);
}

void pn51x_process_done(struct pn51x_request *req)
{
    struct pn51x_common *pn51x = container_of(req, struct pn51x_common, request);

    pn51x_reg_write(CommandReg, CMD_IDLE);
    pn51x_reg_set(ControlReg, TStopNow);
    pn51x_reg_clear(TModeReg, TAuto);
    pn51x_reg_write(CommIRqReg, 0x7F);

    OS_Event_set(pn51x->pn51x_event,COMPLETE_EVENT);
}

void pn51x_process_request(struct pn51x_request *req)
{
    struct pn51x_common *pn51x = container_of(req, struct pn51x_common, request);

    pn51x->intr_enable_mask = IRqInv|ErrIEn|TimerIEn;
    req->done = pn51x_process_done;

    ASSERT(OS_Mutex_lock(pn51x->pn51x_lock) == OS_MUTEX_OK);

    pn51x_reg_write(BitFramingReg, req->bit_frame);
    pn51x_reg_write(FIFOLevelReg, FlushBuffer);		// flush fifo

    if(req->time_out) {
        //if(req->timer_start_auto)
        set_pn51x_timer((uint16_t)req->time_out);
        if(req->timer_start_now)
            pn51x_reg_set(ControlReg, TStartNow);
        }

        if(req->command == CMD_MFAUTHENT)
		pn51x->intr_enable_mask |= IdleIEn;
#if 0
        if(req->direction == RECEIVE) {
            pn51x->intr_enable_mask |= RxIEn|HiAlertIEn;
            pn51x_reg_set(REG_COMMIRQ, BIT_HIALERTIRQ);
        }
        else {
            if(req->length > PN512_FIFO_MAX_FIFO_ALLOWED) {
                pn51x_fifo_write(&req->buf[req->actual], PN51X_FIFO_MAX_FIFO_ALLOWED);
                req->actual += PN51X_FIFO_MAX_FIFO_ALLOWED;
                req->length -= PN51X_FIFO_MAX_FIFO_ALLOWED;
                pn51x->intr_enable_mask |= LoAlertIEn;
                pn51x_reg_set(REG_COMMIRQ, BIT_LOALERTIRQ);
        }
        else {
            pn51x_fifo_write(&req->buf[req->actual], req->length);
            req->actual = req->length = 0;
            pn51x->intr_enable_mask |= TxIEn;
            if(req->direction == TRANSCEIVE)
                pn51x->intr_enable_mask |= RxIEn;
            }
        }
        pn51x_reg_write(REG_COMMAND, req->command);
        if(req->direction == TRANSCEIVE) {
            pn51x_reg_set(REG_BITFRAMING, BIT_STARTSEND);
        }
#else
        if(req->direction == RECEIVE) {
            pn51x->intr_enable_mask |= RxIEn|HiAlertIEn;
            pn51x_reg_set(CommIRqReg, HiAlertIRq);
            pn51x_reg_write(CommandReg, req->command);
        }
        else {
            if(req->length > PN51X_FIFO_MAX_FIFO_ALLOWED) {
                uint8_t temp_len;

                pn51x_fifo_write(req->buf, PN51X_FIFO_MAX_FIFO_ALLOWED);
                req->actual = PN51X_FIFO_MAX_FIFO_ALLOWED;
                req->length -= PN51X_FIFO_MAX_FIFO_ALLOWED;

                pn51x_reg_write(CommandReg, req->command);

                pn51x_reg_set(BitFramingReg, StartSend);

                while(req->length) {
                    if(pn51x_reg_read(FIFOLevelReg) < pn51x->water_level) {
                        pr_debug("water=%d\n", pn51x->water_level);
                        temp_len = (PN51X_FIFO_MAX_FIFO_ALLOWED - pn51x->water_level);
                        temp_len = req->length < temp_len ? req->length : temp_len;
                        pn51x_fifo_write(&req->buf[req->actual], temp_len);
                        req->actual += temp_len;
                        req->length -= temp_len;
                    }	
                }

                req->actual = req->length = 0;
                pn51x->intr_enable_mask |= TxIEn;
                if(req->direction == TRANSCEIVE)
                    pn51x->intr_enable_mask |= RxIEn;
            }
            else {
                pn51x_fifo_write(req->buf, req->length);
                req->actual = req->length = 0;

                pn51x->intr_enable_mask |= TxIEn;
                if(req->direction == TRANSCEIVE)
                pn51x->intr_enable_mask |= RxIEn;

                pn51x_reg_write(CommandReg, req->command);

                pn51x_reg_set(BitFramingReg, StartSend);
		}
        }
#endif
	
    // enable pn51x irq
    pn51x_reg_set(CommIEnReg, pn51x->intr_enable_mask);

    // wait for event complete
    if (MQX_OK != OS_Event_wait(pn51x->pn51x_event,COMPLETE_EVENT,0,0)) {
        pr_err("wait complete event fail");
    }

    // disable pn51x irq
    pn51x_reg_write(CommIEnReg, IRqInv);

    ASSERT(OS_Mutex_unlock(pn51x_dev->pn51x_lock) == OS_MUTEX_OK);
	
}

static void pn51x_interrupt( void * data)
{
    struct pn51x_common *pn51x = (struct pn51x_common *)data;
#ifdef HANDLE_INTR_IN_TASK
    if(lwgpio_int_get_flag((LWGPIO_STRUCT_PTR) (&pn51x->intr_pin))) {
        printf("pn51x ext intr\n");
        OS_Event_set(pn51x->pn51x_event,EXT_INTR_EVENT);
        lwgpio_int_clear_flag((LWGPIO_STRUCT_PTR) (&pn51x->intr_pin));
    }
#else
    struct pn51x_request *req = &pn51x->request;
    uint8_t comm_irq;

    pn51x_reg_write(CommIEnReg, IRqInv);
    // printf("intr TModeReg 0x%x\n",pn51x_reg_read(TModeReg)); // test read reg in interrupt

    comm_irq = pn51x_reg_read(CommIRqReg);

    // reset irq mark
    pn51x_reg_write(CommIRqReg, comm_irq);
    comm_irq &= pn51x->intr_enable_mask;

    // indicate the timer decrements the TimerValue Register to zero
    if(comm_irq & TimerIRq) {
        // stop timer // at done callback (ControlReg, TStopNow)
        req->error_code = -ERROR_NOTAG;
        req->done(req);
        goto done;
    }

    // indicate any error bit in the Error Register is set
    if(comm_irq & ErrIRq) {
        uint8_t error = pn51x_reg_read(ErrorReg);
        if(BITISSET(error, CollErr))	
            req->error_code = -ERROR_COLL;				// collision detected
        else {
            if(BITISSET(error, ParityErr)) 
            req->error_code = -ERROR_PARITY;			// parity error
        }

        if(BITISSET(error, ProtocolErr))
            req->error_code = -ERROR_PROTOCOL;		// framing error

        if(BITISSET(error, BufferOvfl) ) {
            pn51x_reg_write(FIFOLevelReg, 0x80);		// FIFO overflow
            req->error_code = -ERROR_BUFOVFL;
        }
        
        if(BITISSET(error, CRCErr))
            req->error_code = -ERROR_CRC;
    }

    // indicate the last bit of the transmitted data was sent out
    if(comm_irq & TxIRq) {
        if(!req->length) {
            // transfer has complete
            req->tx_done = 1;
            if(req->direction == TRANSMIT) {
                req->done(req);
                goto done;
            }
            else { /* TRANSCEIVE or RECEIVE */
                pn51x->intr_enable_mask &= ~(LoAlertIEn|TxIEn);
                pn51x->intr_enable_mask |= RxIEn|HiAlertIEn;
                pn51x_reg_write(CommIEnReg, pn51x->intr_enable_mask);
            }
        }
    }

    // indicate bit HiAlert in register Status1Reg is set
    if(comm_irq & HiAlertIRq) {
        uint8_t level;
        if(!req->tx_done)
            return;
        // receiving stage
        level = pn51x_reg_read(FIFOLevelReg);
        pn51x_fifo_read(&req->buf[req->actual], level);
        req->actual += level;
    }

    // indicate the receiver detects the end of a valid datastream
    if(comm_irq & RxIRq) {
        // receiving has complete
        uint8_t level;
        level = pn51x_reg_read(FIFOLevelReg);
        pn51x->intr_enable_mask &= ~HiAlertIEn;
        pn51x_reg_write(CommIEnReg, pn51x->intr_enable_mask);
        req->rx_last_bits = pn51x_reg_read(ControlReg) & RxLastBitsMask;

        pn51x_fifo_read(&req->buf[req->actual], level);
        req->actual += level;
        if(req->rx_last_bits)
            req->bit_numbers = (req->actual - 1) * 8 + req->rx_last_bits;
        else
            req->bit_numbers = req->actual * 8;

        req->done(req);

        goto done;
    }

    if(comm_irq & IdleIRq) {
        req->done(req);
        goto done;
    }

#if 0
    // bit LoAlert in register Status1Reg is set
    if(comm_irq & LoAlertIRq) {
        UINT8 lim_len;
        if(!req->tx_done) {
            // transfer stage
            lim_len = PN51X_FIFO_MAX_DEEPTH - pn51x->water_level;
            if(req->length > lim_len) {
                pn51x_fifo_write(&req->buf[req->actual], lim_len);
                req->actual += lim_len;
                req->length -= lim_len;
                if((pn51x_reg_read(REG_STATUS2)&ModemStateMask) == 0x01) {
                    pn51x_reg_set(REG_BITFRAMING, BIT_STARTSEND);
                }
            }
            else if(req->length) {
                pn51x_fifo_write(&req->buf[req->actual], req->length);
                req->actual = req->length = 0;

                pn51x->intr_enable_mask &= ~LoAlertIEn;
                pn51x->intr_enable_mask |= TxIEn;
                if(req->direction == TRANSCEIVE)
                pn51x->intr_enable_mask |= RxIEn;

                pn51x_reg_write(CommIEnReg, pn51x->intr_enable_mask);				
                pn51x_reg_set(REG_BITFRAMING, BIT_STARTSEND);
		}
        }
    }
#endif

done:
	pn51x_reg_write(CommIEnReg, pn51x->intr_enable_mask);
#endif    
}

#ifdef HANDLE_INTR_IN_TASK
static uint32_t _nfc_task_create(void * data) 
{
    _task_id task_id;
    TASK_TEMPLATE_STRUCT task_template;

    struct pn51x_common *pn51x = (struct pn51x_common *)data;
    ASSERT(pn51x);

    /* create task for processing interrupt deferred work */
    task_template.TASK_TEMPLATE_INDEX = NFC_TASK_TEMPLATE_INDEX;
    task_template.TASK_ADDRESS = NFC_TASK_ADDRESS;
    task_template.TASK_STACKSIZE = NFC_TASK_STACKSIZE;
    task_template.TASK_PRIORITY = NFC_TASK_PRIORITY;
    task_template.TASK_NAME = NFC_TASK_NAME;
    task_template.TASK_ATTRIBUTES = NFC_TASK_ATTRIBUTES;
    task_template.CREATION_PARAMETER = (uint32_t)data;
    task_template.DEFAULT_TIME_SLICE = NFC_TASK_DEFAULT_TIME_SLICE;

    task_id = _task_create_blocked(0, 0, (uint32_t)&task_template);
    
    if (task_id == 0) {
        return MQX_ERROR;
    }
    pn51x->task_handle = task_id;
    _task_ready(_task_get_td(task_id));

    return MQX_OK;
}

static void _nfc_intr_task(void * data) 
{
    struct pn51x_common *pn51x = (struct pn51x_common *)data;
    ASSERT(pn51x);

    struct pn51x_request *req = &pn51x->request;
    uint8_t comm_irq;
    
    while (1) {
        if (OS_EVENT_OK == OS_Event_wait(pn51x->pn51x_event,EXT_INTR_EVENT|CLOSE_TASK_EVENT,0,0)) {

            pn51x_reg_write(CommIEnReg, IRqInv);
            // printf("intr TModeReg 0x%x\n",pn51x_reg_read(TModeReg));

            if(OS_Event_check_bit(pn51x->pn51x_event,CLOSE_TASK_EVENT)) {
                printf("nfc task exited\n");
                return;
            }

            comm_irq = pn51x_reg_read(CommIRqReg);

            // reset irq mark
            pn51x_reg_write(CommIRqReg, comm_irq);
            comm_irq &= pn51x->intr_enable_mask;

            // indicate the timer decrements the TimerValue Register to zero
            if(comm_irq & TimerIRq) {
                // stop timer // at done callback (ControlReg, TStopNow)
                req->error_code = -ERROR_NOTAG;
                req->done(req);
                goto done;
            }

            // indicate any error bit in the Error Register is set
            if(comm_irq & ErrIRq) {
                uint8_t error = pn51x_reg_read(ErrorReg);
                if(BITISSET(error, CollErr))	
                    req->error_code = -ERROR_COLL;				// collision detected
                else {
                    if(BITISSET(error, ParityErr)) 
                        req->error_code = -ERROR_PARITY;			// parity error
                }

                if(BITISSET(error, ProtocolErr))
                    req->error_code = -ERROR_PROTOCOL;		// framing error

                if(BITISSET(error, BufferOvfl) ) {
                    pn51x_reg_write(FIFOLevelReg, 0x80);		// FIFO overflow
                    req->error_code = -ERROR_BUFOVFL;
                }

                if(BITISSET(error, CRCErr))
                    req->error_code = -ERROR_CRC;
            }

            // indicate the last bit of the transmitted data was sent out
            if(comm_irq & TxIRq) {
                if(!req->length) {
                    // transfer has complete
                    req->tx_done = 1;
                    if(req->direction == TRANSMIT) {
                        req->done(req);
                        goto done;
                    }
                    else { /* TRANSCEIVE or RECEIVE */
                        pn51x->intr_enable_mask &= ~(LoAlertIEn|TxIEn);
                        pn51x->intr_enable_mask |= RxIEn|HiAlertIEn;
                        pn51x_reg_write(CommIEnReg, pn51x->intr_enable_mask);
                    }
                }
            }

            // indicate bit HiAlert in register Status1Reg is set
            if(comm_irq & HiAlertIRq) {
                uint8_t level;
                if(!req->tx_done)
                    return;
                // receiving stage
                level = pn51x_reg_read(FIFOLevelReg);
                pn51x_fifo_read(&req->buf[req->actual], level);
                req->actual += level;
            }

            // indicate the receiver detects the end of a valid datastream
            if(comm_irq & RxIRq) {
                // receiving has complete
                uint8_t level;
                level = pn51x_reg_read(FIFOLevelReg);
                pn51x->intr_enable_mask &= ~HiAlertIEn;
                pn51x_reg_write(CommIEnReg, pn51x->intr_enable_mask);
                req->rx_last_bits = pn51x_reg_read(ControlReg) & RxLastBitsMask;

                pn51x_fifo_read(&req->buf[req->actual], level);
                req->actual += level;
                if(req->rx_last_bits)
                    req->bit_numbers = (req->actual - 1) * 8 + req->rx_last_bits;
                else
                    req->bit_numbers = req->actual * 8;

                req->done(req);

                goto done;
            }

            if(comm_irq & IdleIRq) {
                req->done(req);
                goto done;
            }

        done:
    	    pn51x_reg_write(CommIEnReg, pn51x->intr_enable_mask);
        }
        else {
            pr_err("wait interrupt event fail\n");
        }
    }
}
#endif

int pn51x_init(struct pn51x_request **req)
{
    int ret;

    pn51x_dev = OS_Mem_alloc_zero(sizeof(struct pn51x_common));
    if(!pn51x_dev) {
        pr_err("fail to requesting memory for pn51x");
        ret = -ENOMEM;
        goto err1;
    }

    pn51x_dev->water_level = PN51X_FIFO_WATER_LEVEL;

    pn51x_dev->pn51x_lock = OS_Mutex_create();
    pn51x_dev->pn51x_event = OS_Event_create(1/* auto clear */);

    ret = nfc_spi_init(&pn51x_dev->spi_handle);
    if(ret)
        goto err2;

    ASSERT(pn51x_dev->spi_handle);

    ret = pn51x_reg_read(VersionReg);
    printf("pn51x version %d\n",ret);

    // pn51x hardware reset (spec P99 the signal must be LOW for at least 100 ns )
    //----  400us(min)-------- 
    //     |__________| low
    //
    pn51x_reset();


    pn51x_reg_write(CommIRqReg, 0x10);
    // software reset pn512
    ret = pn51x_reg_write(CommandReg, CMD_SOFTRESET);
    if(ret < 0)
        goto err2;
    while((pn51x_reg_read(CommIRqReg)&IdleIRq) == 0);  /* wait soft reset command finish.*/

    //pn51x_reg_read(REG_MODE);
    //pn51x_reg_read(REG_MODE);
    ret = pn51x_reg_write(TxControlReg, 0x00);           //Turn off the Antenna
    if(ret < 0)
        goto err2;

    ret = pn51x_reg_write(CommandReg, 0x00);           // Switch on the analog part of the receiver 
    if(ret < 0)
        goto err2;

    ret = pn51x_reg_write(ControlReg, 0x10);              // Set PN512 in initiator mode
    if(ret < 0)
        goto err2;

    ret = pn51x_reg_write(FIFOLevelReg, 0x80);          // flush FIFO
    if(ret < 0)
        goto err2;

    ret = pn51x_reg_write(WaterLevelReg, pn51x_dev->water_level);         // set water level
    if(ret < 0)
        goto err2;

    //while(1){turn_on_antenna();}
    pn51x_reg_write(CommIEnReg, 0x80);	   // disable interrupts
    pn51x_reg_write(DivIEnReg, 0x00);
    pn51x_reg_write(CommIRqReg, 0x7F);         // clean all the irq flag bits
    pn51x_reg_write(DivIRqReg, 0x1F);

#ifdef HANDLE_INTR_IN_TASK
    ASSERT(_nfc_task_create(pn51x_dev) == MQX_OK);
#endif
    // pn51x ext pin interrupt init and enable it.
    // TODO if error goto err4;
    pn51x_init_extintr();
    
    *req = &pn51x_dev->request;

    // pn512_reg_read(REG_MODE);
    return(0);

//err4:
//err3:
err2:
	OS_Mem_free(pn51x_dev);
err1:
	return	ret;
}

int pn51x_uninit(void)
{
    pn51x_reg_write(TxControlReg, 0x00);     // turn off antenna

    // disable ext pin interrupt
    // TODO
#ifdef HANDLE_INTR_IN_TASK
    //_nfc_task_destroy(pn51x_dev);
    if(pn51x_dev->task_handle) {
      OS_Event_set(pn51x_dev->pn51x_event ,CLOSE_TASK_EVENT);
      OS_Time_delay(5);
      OS_Task_delete(pn51x_dev->task_handle);
    }
#endif

    if(pn51x_dev->pn51x_lock) {
        OS_Mutex_destroy(pn51x_dev->pn51x_lock);
        pn51x_dev->pn51x_lock = NULL;
    }
    if(pn51x_dev->pn51x_event){
        OS_Event_destroy(pn51x_dev->pn51x_event);
        pn51x_dev->pn51x_event = NULL;
    }
    
    nfc_spi_uninit(pn51x_dev->spi_handle);

    if(!pn51x_dev)
        OS_Mem_free(pn51x_dev);

    return(0);
}

