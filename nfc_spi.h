#include <mqx.h>
#include <bsp.h>
#include <spi.h>


int nfc_spi_init(void ** spi_fd);

void nfc_spi_uninit(void * spi_fd);

int nfc_spi_write(void * spi, uint8_t *in_buf, uint32_t in_len);

int nfc_spi_write_then_read(void * spi, uint8_t *in_buf, uint32_t in_len,
										uint8_t *out_buf, uint32_t out_len);






