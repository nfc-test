
#ifndef PART4_H
#define PART4_H

#include "common.h"

#define SLOTERROR_T1_3RETRY_FAIL_RESYNCH_PASS  31
#define SLOTERROR_T1_3RETRY_FAIL_RESYNCH_FAIL  30
#define SLOTERROR_T1_CHECKSUM_ERROR            29
#define SLOTERROR_T1_OTHER_ERROR               28
#define SLOTERROR_T1_LEN_INF_CONFLICT          27

#define SLOTERROR_TCL_3RETRANSMIT_FAIL  63
#define SLOTERROR_TCL_3RETRY_TIMEOUT    62
#define SLOTERROR_TCL_BLOCK_INVALID     61

#define APDURECLENTHREHOLD    512

extern const uint16_t fsdi_to_fsd[];

int typeA_request_ats(struct picc_device *picc);
void typeA_set_timeout(struct picc_device *picc, uint8_t timeout);
uint8_t typeA_speed_check(struct picc_device *picc);
void typeA_high_speed_config(struct picc_device *picc, uint8_t speedParam, uint8_t typeB);
void typeA_prologue_feild_load(struct picc_device *picc);
void typeA_pps_check_and_send(struct picc_device *picc);
int typeA_select_(struct picc_device *picc, uint8_t blockPCB);
int typeA_deselect_request(struct picc_device *picc);
int typeA_standard_apdu_handler(struct picc_device *picc, uint8_t *cmdBuf, uint32_t senLen, uint8_t *recBuf, uint32_t *recLen, uint8_t *level);


#endif


