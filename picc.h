

#ifndef PICC_H
#define PICC_H

#include "common.h"
#include "ccid.h"
#include "pn51x.h"

//#define FELICA_SUPPORT
//#define TOPAZ_SUPPORT
//#define PCSC_SUPPORT
//#define MIRARE_SUPPORT

// picc type
#define PICC_ABSENT        0xCC
#define PICC_TYPEA_TCL     0x20    // typeA which support ISO/IEC 14443-4
#define PICC_MIFARE        0x10
#define PICC_TYPEB_TCL     0x23    // typeB which support ISO/IEC 14443-4
#define PICC_ST_MEMERY     0x24
#define PICC_TOPAZ         0x04
#define PICC_FELICA212     0x11  
#define PICC_FELICA424     0x12


#define PICC_ERRORCODE_NONE					RDR_TO_PC_SLOTERROR_NONE
#define PICC_ERRORCODE_CMD_ABORTED			RDR_TO_PC_SLOTERROR_CMD_ABORTED
#define PICC_ERRORCODE_MUTE					RDR_TO_PC_SLOTERROR_ICC_MUTE
#define PICC_ERRORCODE_XFR_PARITY_ERROR		RDR_TO_PC_SLOTERROR_XFR_PARITY_ERROR
#define PICC_ERRORCODE_XFR_OVERRUN			RDR_TO_PC_SLOTERROR_XFR_OVERRUN
#define PICC_ERRORCODE_HW_ERROR				RDR_TO_PC_SLOTERROR_XFR_OVERRUN

#define PICC_ICCSTATUS_ACTIVE				RDR_TO_PC_ICCSTATUS_ACTIVE
#define PICC_ICCSTATUS_INACTIVE				RDR_TO_PC_ICCSTATUS_INACTIVE
#define PICC_ICCSTATUS_NOICC				RDR_TO_PC_ICCSTATUS_NOICC


struct picc_device;

struct pcd_device {

#define TYPEA			(1<<0)		// bit 0: typeA
#define TYPEB			(1<<1)		// bit 1: typeB
#define FELICA212		(1<<2)		// bit 2: felica212
#define FELICA414		(1<<3)		// bit 3: felica414
#define TOPAZ			(1<<4)		// bit 4: topaz
    uint8_t	support_card_type;

    uint8_t	FSDI;        // codes FSD, FSD defines the maximum size of a fram the PCD is able to receive, default 8, 256 bytes( refer to FSCConvertTbl[])
    uint8_t	max_speed;    // Maximum communication speed supported by the IFD
    uint8_t	current_speed;
    
    BOOL		piccPoll;
    // describes prologue filed in Block Format,  ISO/IEC 14443-4
    uint8_t	PCB;
    uint8_t	CID;
    uint8_t	NAD;


#define AUTO_RATS				(1<<0)	// bit 0: Auto RATS
#define AUTO_POLLING			(1<<1)	// bit 1: Auto poll
#define POLLING_CARD_ENABLE	(1<<2)	// bit 2: poll card
    uint8_t			flags_polling;

    uint8_t			reserve1;

    uint16_t			poll_interval;

    uint8_t			mifare_key[2][6];

    struct picc_device		*picc;
}/* __attribute__((packed, aligned(1)))*/;

enum picc_state
{
	PICC_POWEROFF,
	PICC_IDLE,
	PICC_READY,
	PICC_SELECTED,
	PICC_ACTIVATED,
	PICC_UPDATE,
};

struct picc_device {

    enum picc_state		states;    // picc states 
    
#define PRESENT			(1<<0)    // bit 0: present
#define ACTIVATED		(1<<1)    // bit 1: activated
#define CONNECTED		(1<<2)    // bit 2: connected
#define FIRST_INSERT        (1<<3)    // bit 3: first insert
#define SLOT_CHANGE	(1<<4)    // bit 4: slot change
    volatile uint8_t		status;

    uint8_t 				type;      // picc type
    uint8_t 				support_part4;    // if support ISO14443-4
    uint8_t 				sn_len;
    
    uint8_t 				sn[10];    // used to stored UID(type A, MAX 10 bytes), PUPI(type B, 4 bytes), NFCID2(felica, 8 bytes)
    uint8_t                         reserve[2];

    char	       			*name;

    // typeA parameters
    uint8_t 				ATQA[2];
    uint8_t 				SAK;
    uint8_t                         reserve1;

    // typeB parameters
    uint8_t 				ATQB[13];
    uint8_t 				ATQB_len;
    uint8_t                         reserve3[2];
    
    
    uint8_t 				attrib_param[4];
    uint8_t 				attrib_response[7];
    uint8_t                         reserve4;


    // felica parameters
    uint8_t 				PAD[8];    // used to stored PMm(manufacture parameter, 8 bytes) of felica
    uint8_t 				system_code[2];    

    // the struct defines these parameter of the PICC which supports ISO14443-4
    uint8_t 				FWI;         // codes FWT, Frame waiting Time Integer, defines the maximum time for a PICC to start its response after the end of a PCD frame
    uint8_t 				SFGI;        // codes amultiplier value used to define the SFGT, SFGT defines guard time needed by the PICC before it is ready to receive the next fram after it has sent the ATS
    
    uint8_t 				FSCI;        // codes FSC, FSC defines the maximum size of a fram accepted by the PICC, default 2, 32 bytes( refer to FSCConvertTbl[])
    uint8_t 				speed;       // stored TA(1), specify the divisor D for each direction

    uint16_t     			FSC;

    uint8_t 				ATS[20];     // an array used to stores the ATS send from PICC


#define PCD_BLOCK_NUMBER		(1<<0)    // bit 0: pcd block number
#define CID_PRESENT				(1<<1)    // bit 1: cid  present
#define PCD_CHAINING			(1<<2)    // bit 2: pcd chaining
#define PICC_CHAINING			(1<<3)    // bit 3: picc chaning
#define WTX_REQUEST				(1<<4)    // bit 4: wtx request
#define TYPEB_ATTRIB			(1<<5)    // bit 5: typeB attrib
#define WTX_REQ_BEFORE			(1<<6)    // bit 6: wtx request before
#define NAD_PRESENT				(1<<7)    // bit 7: NAD present
    uint8_t 				flags_TCL;    // flag for 14443-4

    uint8_t 				WTXM;
    // prologue filed in Block Format,  ISO/IEC 14443-4
    uint8_t 				PCB;
    uint8_t 				NAD;

    uint8_t 				CID;
    uint8_t                         reserve5[3];

    // mifare
    volatile uint8_t			work_key[6];
    volatile uint8_t			key_valid;
    volatile uint8_t			key_type;
    
    volatile uint8_t			key_No;
    volatile uint8_t			authen_need;
    volatile uint8_t			block;

	// pcsc
#define TXCRC			(1<<0)    // bit 0: TxCRC bit
#define RXCRC			(1<<1)    // bit 1: RxCRC bit
#define TXPARITY		(1<<2)    // bit 2: TxParity bit
#define RXPARITY		(1<<3)    // bit 3: RxParity bit
#define PROLOGUE		(1<<4)    // bit 4: prologue bit
#define VENDORSPEC		(1<<5)    // bit 5: vendor specify bit
    volatile uint8_t 		flags_tx_rx;

    volatile uint8_t 		previous_cmd;
    volatile uint8_t		transfer_status;
    volatile uint8_t		flags_status;
    volatile uint8_t		last_rx_valid_bits;

    volatile uint8_t		last_tx_valid_bits;
    volatile uint8_t		next_cmd;
    uint8_t                         reserve6[2];

    struct pn51x_request	*request;
    struct pcd_device		*pcd;
} /*__attribute__((packed, aligned(1)))*/;


extern const uint8_t IFDVersion[];

extern const uint16_t FSCConvertTbl[9];


void picc_wait_for_req(struct pn51x_request *req);
uint8_t get_cid(uint8_t *uid);
void picc_reset(struct picc_device *picc);

 int picc_power_on(struct picc_device *picc, uint8_t *atrBuf, uint32_t *atrLen);
 void picc_power_off(struct picc_device *picc);
 
 int picc_command_exchange(struct picc_device *picc, uint8_t *cmdBuf, uint32_t cmdLen, uint8_t *resBuf, uint32_t *resLen, uint8_t *level);

#endif


